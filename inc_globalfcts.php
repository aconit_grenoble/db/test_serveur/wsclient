<?php
# v5.0		2013-08-06	PhD	Refonte sur XMLRPC-v3.0.0	
# v6.0		2022-09-24	PhD	Supprimé each dans xmlrpcval_decode (PHP 8.0); 'debug' local devient 'debugx'
# v6.2		2026-07-07	PhD	Remis debux en place
###

#------------------------------------------------------------------------------ debugx ---
function debugx ($code, $msg, $var="", $mix="") {
// Affiche un message et (éventuellement) le contenu d'une variable 
// si un bit positionné de la  variable $debugx correspond à un bit de $code.
// Traite les variables simples et les tableaux.
	global $debugx;
	if ($debugx & $code) {
		echo "<pre>[[[".$msg ;
		if ($var) {
			echo " : ";
			if (is_array ($var) || is_numeric ($var) || empty ($mix) ) {			
				print_r ($var); 
			} else  {
				for ($i=0; $i<strlen($var); $i+=1) {
					echo ".".substr ($var, $i, 1);
				}
			}			
		} 
		echo "]]]</pre>";
	}
	return;
}

#------------------------------------------------------------------- xmlrpcval_decode ---
/*Fonction de recuperation des donnees recues
 *du serveur en XML-RPC
 *Transforme $val du type xmlrpcval
 *en un (tableau (de tableaux) de) string(s)
 *Recursive */
function xmlrpcval_decode ($val) {
    switch ($val->kindOf()) {
	    case "array":
	        /*on recupere un array en tableau indexe*/
	    	$ret=array();
	        for ($i=0;$i<$val->arraysize();$i++) {
			    $ret[$i]=xmlrpcval_decode($val->arraymem($i));
			}
			break;
			
	    case "struct":
	      /*une structure devient un tableau associatif*/
	    	$ret=array();
	
				foreach ($val as $key => $value) {
					$ret[$key]=xmlrpcval_decode($value);
				}
			break;
			
	    default:
	        /*un scalaire est transforme en string*/
	        $ret=$val->scalarval();
	}
	return $ret;
}



?>
