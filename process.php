<?php
# V1.1	2010-04-07	PhD	Replacé l'en tête HTML après ouverture de session
# V2.0	2010-05-22	PhD	Ajout du mode "recherche2" dans XML-RPC
# v3.0	2010-05-24	PhD	Restructuration 
# v4.0	2012-06-01	PhD	Corrections des paramètres	
# v4.01	2012-06-04	PhD	Regroupe tout le traitement des méthodes
# v4.1	2012-06-11	PhD	Ajout methode "recherche2etat2"
# v4.2	2012-08-11	PhD	Ajouté controle de l'error reporting
# v5.0	2013-08-07	PhD	Refonte sur XMLRPC-v3.0.0	
# v6.0	2022-08-17	PhD	Refonte sur XMLRPC-V4.6.0 ; suppression test protocole SOAP
# v6.2	2023-07-05	PhD	Ajout des méthodes Museum'IT
# v7.0	2023-07-20	PhD	Refonte processus d'identification et format des méthodes
# v7.3	2024-01-17	PhD	Modification des libellés ItInHeritage
###

require_once ('./inc_globalvars.php');
require_once("./inc_globalfcts.php");

// Use the custom class autoloader.
include_once __DIR__ . '/phpxmlrpc/Autoloader.php';
PhpXmlRpc\Autoloader::register();

if (!@$_SESSION) {session_start();}
$adrserver = $_SESSION["adrserver"];


// Envoi de l'en tête HTML -----------------------------------------------------
echo $header;
echo <<<EOD
<body>
	<img src="l_aconit.png" alt="Logo Aconit" />
	<h1>Réponse du serveur WebService DBAconit</h1>
	<h3 style="float:right;">URL : $adrserver </h3>
EOD;

//debugx (255, 'POST', $_POST);
	// Mémoriser les retours du formulaires
		if (isset($_POST["ack"]))					$_SESSION["ack"] = $_POST["ack"];
		if (isset($_POST["chaine"]))			$_SESSION["chaine"] = $_POST["chaine"];
		if (isset($_POST["chaine2"]))			$_SESSION["chaine2"] = $_POST["chaine2"];
		if (isset($_POST["chaine3"]))			$_SESSION["chaine3"] = $_POST["chaine3"];
		if (isset($_POST["chaine4"]))			$_SESSION["chaine4"] = $_POST["chaine4"];
		if (isset($_POST["date_pivot"]))	$_SESSION['date_pivot'] = $_POST["date_pivot"];
		if (isset($_POST["date_pivot2"]))	$_SESSION['date_pivot2'] = $_POST["date_pivot2"];
		if (isset($_POST["etat"]))			$_SESSION["etat"] = $_POST["etat"];
		if (isset($_POST["idcol"]))			$_SESSION["idcol"] = $_POST["idcol"];
		if (isset($_POST["idcol1"]))		$_SESSION["idcol1"] = $_POST["idcol1"];
		if (isset($_POST["idcol2"]))		$_SESSION["idcol2"] = $_POST["idcol2"];
		if (isset($_POST["idcol3"]))		$_SESSION["idcol3"] = $_POST["idcol3"];
		if (isset($_POST["login"]))			$_SESSION["login"] = $_POST["login"];
		if (isset($_POST["method"]))		$_SESSION["method"] = $_POST["method"];
		if (isset($_POST["ndb"]))				$_SESSION["ndb"] = $_POST["ndb"];
		if (isset($_POST["nrinv"]))			$_SESSION["nrinv"] = $_POST["nrinv"];
		if (isset($_POST["passe"]))			$_SESSION["passe"] = $_POST["passe"];
		if (isset($_POST["type"]))			$_SESSION["type"] = $_POST["type"];
		if (isset($_POST["type2"]))			$_SESSION["type2"] = $_POST["type2"];
		if (isset($_POST["type3"]))			$_SESSION["type3"] = $_POST["type3"];
//debugx (255, 'SESSION', $_SESSION);
    //creation du client
  $adrserver.="serverxmlrpc.php";
	$client = new PhpXmlRpc\client($adrserver);

	//gestion du mode debug
	if (isset($_POST["debug"])) {
		$client->setDebug(2);
		echo "<h2>Traces de debug</h2>";
	}

	// Préparation du nom de fichier session sur le serveur
	$_SESSION['fnsession'] = $_SESSION['login'].$_SESSION['ndb'];
	
	//creation de la requete
	switch ($_SESSION["method"]) {
																// Méthodes générales
	case 'login' :
		$message=new PhpXmlRpc\Request('login', 
			array(new PhpXmlRpc\Value($_SESSION["ndb"],"string"), 
				new PhpXmlRpc\Value($_SESSION["login"],"string"),
				new PhpXmlRpc\Value($_SESSION["passe"],"string")
			));	
		break;
		
	case 'logout' :
		$message=new PhpXmlRpc\Request('logout', 
			array(new PhpXmlRpc\Value($_SESSION["fnsession"],"string") 
			));	
		$_SESSION["fnsession"] = '';
		break;
		
	case 'recherche' :
		$message=new PhpXmlRpc\Request('recherche',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
				new PhpXmlRpc\Value($_SESSION["chaine"],"string")
			));
		break; 
		
	case 'recherche_type' :
		$message=new PhpXmlRpc\Request('recherche_type',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
				new PhpXmlRpc\Value($_SESSION["chaine2"],"string"),
				new PhpXmlRpc\Value($_SESSION["type"],"string")
			));
		break; 
				
	case 'recherche_etat2' :
		$message=new PhpXmlRpc\Request('recherche_etat2',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
				new PhpXmlRpc\Value($_SESSION["chaine3"],"string"),
				new PhpXmlRpc\Value($_SESSION["type3"],"string")
			));
		break; 
																// Format réponse tableau
		case 'fiche' :
			$message=new PhpXmlRpc\Request('fiche',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
				new PhpXmlRpc\Value($_SESSION["nrinv"],"string")
			));
		break; 
			
	case 'ficheid' :
		$message=new PhpXmlRpc\Request('ficheid',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
				new PhpXmlRpc\Value($_SESSION["idcol"],"string")
			));
		break; 
																// Format XML spécifique PATSTEC
	case 'pstc_stats' :
		$message=new PhpXmlRpc\Request('pstc_stats',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
		));
		break;

	case 'pstc_list' :
		$message=new PhpXmlRpc\Request('pstc_list',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
			new PhpXmlRpc\Value($_SESSION["etat"],"string"),
		));
		break;

	case 'pstc_trans' :
		$message=new PhpXmlRpc\Request('pstc_trans',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
			new PhpXmlRpc\Value($_SESSION["idcol1"],"string"),
		));
		break;

	case 'pstc_ack' :
		$message=new PhpXmlRpc\Request('pstc_ack',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
			new PhpXmlRpc\Value($_SESSION["idcol2"],"string"),
			new PhpXmlRpc\Value($_SESSION["ack"],"string")
		));
		break;
																// Format XML spécifique Museum'IT
	case 'mit_list_date' :
		$message=new PhpXmlRpc\Request('mit_list_date',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
			new PhpXmlRpc\Value($_SESSION["date_pivot"],"string"),
		));
		break;

	case 'mit_record_date' :
		$message=new PhpXmlRpc\Request('mit_record_date',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
			new PhpXmlRpc\Value($_SESSION["date_pivot2"],"string"),
		));
		break;

	case 'mit_record_id' :
		$message=new PhpXmlRpc\Request('mit_record_id',
			array(new PhpXmlRpc\Value($_SESSION["fnsession"]),	
			new PhpXmlRpc\Value($_SESSION["idcol3"],"string"),
		));
		break;
																// Ancien format méthode PORTAIL
	case 'recherche2' :
		$message=new PhpXmlRpc\Request('recherche2',
			array(new PhpXmlRpc\Value($_SESSION["login"]),	
			new PhpXmlRpc\Value($_SESSION["chaine4"],"string"),
			new PhpXmlRpc\Value($_SESSION["type"],"string")
		));
		break;

	}

	/*envoi de la requete
	 *affichage les traces de debug si elles sont demandées
	 *on ne peut pas reporter cet affichage (phénomene du au protocole)*/
	$response=$client->send($message);

	//affichage des echanges
	if (isset($_POST['affiche'])) {
	    echo "<h2>Messages &eacute;chang&eacute;s</h2>";
	    echo "<ul><li><h3>Message envoy&eacute;</h3>";
		echo "<div class='donnees'><p>";
		echo str_replace(array('<','>'),array('&lt;','&gt;'),$message->serialize());
		echo "</p></div><hr /></li>";
	    echo "<li><h3>Message re&ccedil;u</h3>";
		echo "<div class='donnees'><p>";
		echo str_replace(array('<','>'),array('&lt;','&gt;'),$response->serialize());
		echo "</p></div><hr /></li></ul>";
	}


	if ($response->faultCode()) {					//gestion des erreurs
    	echo "ERROR: ".$response->faultCode()." : ".$response->faultString();   
	} else {															//decodage de la reponse pour la mettre au format php standard	
		$result= xmlrpcval_decode($response->value());
	}


#*********************************************************************************
//on effectue un  affichage simplifié !!!
 if (isset ($result)) {
    echo "<pre>";
    if (is_string ($result))
      echo (htmlspecialchars($result));
		else print_r ($result);
    echo "</pre>";
	}

# ************************************************************************* FORM 
#--formulaire de consultation des donnees

// Préparer le "check" pour le bouton radio
$tcheck = array();
$tcheck[$_SESSION["method"]] = "checked='true'";
?>
<h1>Requète au web-serveur DBAconit</h1>
 

<form method=post action="process.php">
	<table id="menu" cellspacing='3'>
		<tr>
			<th> &nbsp; </th>
			<th> Méthode </th>
			<th> Param 0 </th>
			<th> Param 1 </th>
			<th> Param 2 </th>
			<th> Fonction </th>
			<th> Utilisé par </th>
		</tr>
		
		<tr>
			<th colspan=6 style="background-color:lightgrey;"> Méthodes générales </th>
			<td> &nbsp;. </td>
		</tr>
			
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['login'] ?> value='login' /> </td>
			<td> login </td>
			<td><input type="text" name="ndb" size="5" value="<?php echo $_SESSION['ndb'] ?>" /> Nr base</td>
			<td> <input type="text" name="login" value="<?php echo $_SESSION['login'] ?>" /> Login</td>
			<td> <input type="password" name="passe" value="<?php echo $_SESSION['passe'] ?>"/> Mot de passe </td>
			<td> Login, ouverture de session </td>
			<td> &nbsp;. </td>
		</tr>
					
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['logout'] ?> value='logout' /> </td>
			<td> logout </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> Fin de dession (facultatif) </td>
			<td> . </td>
		</tr>
		
			<th colspan=6 style="background-color:lightgrey;"> Premières méthodes —  Transfert format tableau </th>
			<td> &nbsp;. </td>
		</tr>
			
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['fiche'] ?> value='fiche' /> </td>
			<td> fiche </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="nrinv" value="<?php echo $_SESSION['nrinv'] ?>"/> No inventaire </td>
			<td> &nbsp; </td>
			<td> appel d'une fiche par No inventaire </td>
			<td> </td>
		</tr>
		
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['ficheid'] ?> value='ficheid' /> </td>
			<td> ficheid </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="idcol" value="<?php echo $_SESSION['idcol'] ?>"/> No ID </td>
			<td> &nbsp; </td>
			<td> appel d'une fiche par No de fiche </td>
			<td> &nbsp; </td>
		</tr>
		
			<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['recherche'] ?> value='recherche' /> </td>
			<td> recherche </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="chaine" value="<?php echo $_SESSION['chaine'] ?>"/> Chaîne recherchée </td>
			<td> &nbsp; </td>
			<td> Recherche d'une chaîne</td>
			<td> &nbsp; </td>
		</tr>
		
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['recherche_type'] ?> value='recherche_type' /> </td>
			<td> recherche_type </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="chaine2" value="<?php echo $_SESSION['chaine2'] ?>"/> Chaîne recherchée </td>
			<td> <input type="text" name="type" value="<?php echo $_SESSION['type'] ?>"/> Types objet </td>
			<td> Recherche d'une chaîne par type d'objet </td>
			<td>  </td>
		</tr>
		
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['recherche_etat2'] ?> value='recherche_etat2' /> </td>
			<td> recherche_etat2 </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="chaine3" value="<?php echo $_SESSION['chaine3'] ?>"/> Chaîne recherchée </td>
			<td> <input type="text" name="type3" value="<?php echo $_SESSION['type3'] ?>"/> Types objet </td>
			<td> Recherche d'une chaîne en état 2  </td>
			<td>  </td>
		</tr>

	<tr>
			<th colspan=6 style="background-color:lightgrey;">Format XML spécifique PATSTEC</th>
			<td> &nbsp;. </td>
		</tr>
			
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['pstc_stats'] ?> value='pstc_stats' /> </td>
			<td> pstc_stats </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> &nbsp; </td>
			<td> &nbsp; </td>
			<td> Statistiques des fiches transmises </td>
			<td> PSTC </td>
		</tr>
		
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['pstc_list'] ?> value='pstc_list' /> </td>
			<td> pstc_list </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="etat" value="<?php echo $_SESSION['etat'] ?>"/> Code état </td>
			<td> &nbsp; </td>
			<td> liste des fiches par état </td>
			<td> PSTC </td>
		</tr>
		
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['pstc_trans'] ?> value='pstc_trans' /> </td>
			<td> pstc_trans </td>
			<td> <?php echo  $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="idcol1" value="<?php echo $_SESSION['idcol1'] ?>"/> Id fiche </td>
			<td> &nbsp; </td>
			<td> Transmission d'une fiche </td>
			<td> PSTC </td>
		</tr>
		
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['pstc_ack'] ?> value='pstc_ack' /> </td>
			<td> pstc_ack </td>
			<td> <?php echo  $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="idcol2" value="<?php echo $_SESSION['idcol2'] ?>"/> Id fiche </td>
			<td> <input type="text" name="ack" value="<?php echo $_SESSION['ack'] ?>"/> Ack 0 ou 1 </td>
			<td> Accusé de réception </td>
			<td> PSTC </td>
		</tr>
		
		<tr>
			<th colspan=6 style="background-color:lightgrey;"> Format XML spécifique Museum'IT </th>
			<td> &nbsp;. </td>
		</tr>
			
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['mit_list_date'] ?> value='mit_list_date' /> </td>
			<td> mit_list_date </td>
			<td> <?php echo $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="date_pivot" value="<?php echo $_SESSION['date_pivot'] ?>"/> Date YYYY-MM-DD </td>
			<td> &nbsp; </td>
			<td> Liste des fiches ITinHeritage modifiées depuis une date donnée </td>
			<td> Museum'IT</td>
		</tr>
		
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['mit_record_date'] ?> value='mit_record_date' /> </td>
			<td> mit_record_date </td>
			<td> <?php echo  $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="date_pivot2" value="<?php echo $_SESSION['date_pivot2'] ?>"/> Date YYYY-MM-DD</td>
			<td> &nbsp; </td>
			<td> Transmission des fiches ITinHeritage modifiées depuis une date donnée </td>
			<td> Museum'IT</td>
		</tr>
		
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['mit_record_id'] ?> value='mit_record_id' /> </td>
			<td> mit_record_id </td>
			<td> <?php echo  $_SESSION['fnsession'] ?> </td>
			<td> <input type="text" name="idcol3" value="<?php echo $_SESSION['idcol3'] ?>"/> No ID </td>
			<td> &nbsp; </td>
			<td>Appel d'une fiche par No fiche </td>
			<td> Museum'IT</td>
		</tr>
		
		<tr>
			<th colspan=6 style="background-color:lightgrey;"> Ancien format méthode ¨PORTAIL </th>
			<td> &nbsp;. </td>
		</tr>
			
		<tr>
			<td><input type='radio' name='method' <?php echo @$tcheck['recherche2'] ?> value='recherche2' /> </td>
			<td> recherche2 </td>
			<td> <?php echo $_SESSION['login'] ?> </td>
			<td> <input type="text" name="chaine4" value="<?php echo $_SESSION['chaine4'] ?>"/> Nom </td>
			<td> <input type="text" name="type2" value="<?php echo $_SESSION['type2'] ?>"/> Type objet </td>
			<td> Recherche d'une chaîne par type d'objet </td>
			<td> PORTAIL </td>
		</tr>
		
	</table>
	<br/>
	<ul>		
		<!--affichage des traces de debug-->
		<li>
			<input type="checkbox" name="debug" value="2" />&nbsp;Mettre le niveau de debug maximum
		</li>
		<!--affichage des requetes-->
		<li>
			<input type="checkbox" name="affiche" value="false" />&nbsp;Afficher les requêtes transmises
		</li>
		<li>
			<input type="submit" name="submit" value="Valider" />
		</li>
	</ul>
</form>
</body>
</html>