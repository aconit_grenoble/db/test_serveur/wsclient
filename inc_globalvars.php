<?php
# v5.0		2013-08-06	PhD	Refonte sur XMLRPC-v3.0.0	
# v6.0		2022-09-24	PhD	Refonte sur XMLRPC-V4.6.0 ; ; 'debug' local devient 'debugx'
# v6.2		2023-07-07	PhD	Ajout des méthodes Museum'IT
# v7.0		2023-07-08	PhD	Refonte de toute dispositif d'identification
# v7.1		2023-08-19	PhD	Refonte headers_list()
# v7.3		2024-01-17	PhD	Modification des libellés ItInHeritage
###

$debugx = 1;
#########

$DBClient = "WSclient v7.3";		// 2024-01-17

$header = '<!DOCTYPE html>
<head>
<title>Client test WebService DBAconit</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="styles.css" />
</head>
'
?>
