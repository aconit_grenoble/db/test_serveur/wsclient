<?php
# v5.0		2013-08-06	PhD	Refonte sur XMLRPC-v3.0.0	
# v6.0		2022-09-06	PhD	Correction adresse serveur local, et serveur distant
# v6.1		2022-10-19	PhD	Ajouté adresse serveur MGSM
# v6.2		2023-06-28	PhD	RàZ 'date_pivot'...
# v7.0		2023-07-20	PhD	Ajout password
# v7.1		2023-08-19	PhD	Nettoyé le formulaire
###

require_once ('./inc_globalvars.php');

if (!isset($_POST['lancement'])) {
###################################################################### Premier affichage ###

echo $header;
?>

<body>
	<img src="l_aconit.png" alt="Logo Aconit" />
	<form method=post action="index.php">
	<h2>Sélection du serveur</h2>
		<ul class="liste_decalee">
		<li><input type='radio' name='serveur' 
				value='http://127.0.0.1/Web_Aconit/db/dbserver/'/> Serveur local PhD</li>
		<li><input type='radio' name='serveur' 
				value='https://db.aconit.org/dbserver/' checked /> Serveur public ACONIT</li>
		<li><input type='radio' name='serveur' 
				value='http://db.musee-sciences-medicales.fr/dbserver/' /> Serveur MGSM</li>
		</ul>
	<p> &nbsp;</p>
	
	<h2>Premier LOGIN</h2>
		<!--on rajoute une marge a cette classe en css-->
		<ul class="liste_decalee">
		
		<!--No de base-->
		<li>No base :&nbsp;
		<input type="text" name="ndb" size="20" value="0" />
		</li>
		
		<!--login-->
		<li>Login :&nbsp;
		<input type="text" name="login" size="20"/>
		</li>
		
		<!--login-->
		<li>Mot de passe :&nbsp;
		<input type='password' name='passe' size='20' maxlength='20' />
		</li>
		
		<li><input  style="margin-left:400px;" type="submit" name="lancement" value="Valider" /></li>
		</ul>
	</form>

	<hr/>
	<?php echo $DBClient; ?>
</body>
</html>

<?php
} else {

########################################## Traitement des valeurs permanentes de session ###
	session_start();

		//stocker le mot de passe et le protocole

		$_SESSION["adrserver"]=$_POST['serveur'];
		$_SESSION["protocol"]='xmlrpc';

		// Et les paramètres permanents
		$_SESSION["login"]=$_POST["login"];
		$_SESSION["passe"]=$_POST["passe"];
		$_SESSION["ndb"]=$_POST["ndb"];

		// Forcer la méthode
		$_SESSION["method"] = 'login';
		
		// Et mettre à blanc les autres paramètres
		$_SESSION["ack"] = '';
		$_SESSION["chaine"] = '';
		$_SESSION["chaine2"] = '';
		$_SESSION["chaine3"] = '';
		$_SESSION["chaine4"] = '';
		$_SESSION["date_pivot"] = '';
		$_SESSION["date_pivot2"] = '';
		$_SESSION["etat"] = '';
		$_SESSION["idcol"] = '';
		$_SESSION["idcol1"] = '';
		$_SESSION["idcol2"] = '';
		$_SESSION["idcol3"] = '';
		$_SESSION["nrinv"] = '';
		$_SESSION["type"] = '';
		$_SESSION["type2"] = '';
		$_SESSION["type3"] = '';
	

		// Et on appelle le traitement standard des appels et réponses
		require_once("process.php");	
	}
?>
